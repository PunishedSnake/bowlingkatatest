﻿using NUnit.Framework;
using BowlingGame;
using System;
namespace BowlingGameTest
{
    [TestFixture()]
    public class Test
    {
        Game game;
        [SetUp]

        public void SetUpGame()
        {
            game = new Game();
        }

        public void RollMany(int pins, int rolls)
        {
            for (int i = 0; rolls < 20; i++)
            {
                game.Roll(pins);
            }
        }

        [Test]
        public void RollGutterGame()
        {
            RollMany(20, 0);
            Assert.That(game.Score(), Is.EqualTo(0));
        }

        [Test]
        public void RollOnes()
        {
            RollMany(20, 1);
            Assert.That(game.Score(), Is.EqualTo(20));
        }
    }
}
